
### Compilation in Docker

```
docker run -it ubuntu:rolling /bin/bash

apt update

apt install -y git

git clone https://github.com/reactos/reactos.git
cd reactos

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update

#Z#
apt install -y clang meson ninja-build mingw-w64 git wget

wget https://svn.reactos.org/amine/RosBEBinFull.tar.gz -O RosBE.tar.gz
tar -xzf RosBE.tar.gz
RosBEBinFull/RosBE.sh
./configure.sh
cd output-MinGW-i386


ninja bootcd -j8
```

